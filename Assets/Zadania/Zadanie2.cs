﻿using System.Collections;
using UnityEngine;

/// Zadanie 2 polega na wykorzystaniu Zadania 1 i tym razem dorobienie płynnego przejścia rotacji i pozycji do docelowych wartości w zadanym czasie 
public class Zadanie2 : Zadanie1
{
    public float Duration;

    private float timer;
    private float Ratio => timer / Duration;
    private bool lockToHand;

    private IEnumerator Start()
    {
        var wait = new WaitForEndOfFrame();
        
        timer = 0;
        lockToHand = false;
        var beginPos = Weapon.transform.position;
        var beginRot = Weapon.transform.rotation;

        while (Duration > timer)
        {
            Weapon.transform.rotation = Quaternion.Lerp(beginRot, HandGrabRot * weaponRotOffset, Ratio);
            Weapon.transform.position = Vector3.Lerp(beginPos, HandGrabber.position - TrueWeaponGrabPoint, Ratio);
            timer += Time.deltaTime;
            yield return wait;
        }

        lockToHand = true;
    }

    protected override void MoveToGrabber()
    {
        if (lockToHand)
            base.MoveToGrabber();
    }
}