﻿public interface ISwitchable
{
    void SwitchOn();
    void SwitchOff();
}