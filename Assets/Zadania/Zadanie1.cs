﻿using UnityEngine;

/// Zadanie 1 polega na ustawieniu pozycji i rotacji GameObjectu Weapon w taki sposób aby WeaponGrabPoint pokrywał sie pozycją i rotacją z HandGrabber
/// Nie ma potrzeby wprowadzania żadnych zmian na scenie, wszystkie potrzebne do rozwiązania referencje są juz wpięte w skrypt
public class Zadanie1 : MonoBehaviour
{
    public GameObject Weapon;
    public Transform WeaponGrabPoint, HandGrabber;

    protected Vector3 weaponPosOffset;
    protected Quaternion weaponRotOffset;
    protected Vector3 TrueWeaponGrabPoint => (Weapon.transform.rotation * weaponPosOffset);
    protected Quaternion HandGrabRot => Quaternion.LookRotation(HandGrabber.forward, HandGrabber.up);

    private void OnEnable()
    {
        weaponPosOffset = Weapon.transform.InverseTransformPoint(WeaponGrabPoint.position);
        weaponRotOffset = Quaternion.Inverse(WeaponGrabPoint.rotation) * Weapon.transform.rotation;
    }

    private void Update()
    {
        MoveToGrabber();
    }

    protected virtual void MoveToGrabber()
    {
        Weapon.transform.rotation = HandGrabRot * weaponRotOffset;
        Weapon.transform.position = HandGrabber.position - TrueWeaponGrabPoint;
    }
}