﻿using System.Collections;
using UnityEngine;

/// Zadanie 3 polega na utworzeniu:
/// Kontrolera drzwi (Zadanie 3)
///     1. Konkretne drzwi muszą się podnosić gdy gracz do nich podejdzie
///     2. Obsługuje otwieranie / zamykanie wszystkich drzwi na raz w momencie wejścia / zejścia na odpowiedni przycisk na mapie (Żółty)
/// Kontrolera oświetlenia (Zadanie 3_1)
///     1. Obsługuje zmianę koloru wszystkich świateł na zielony / czerwony po wejściu / zejściu z przycisku na mapie (Czerwony)
/// Przycisku (Zadanie 3_2), który uruchamia powyższe działania
/// Kontroler drzwi
public class Zadanie3 : MonoBehaviour, ISwitchable
{
    [Header("Door")] [SerializeField] private Transform doorTransform;
    [SerializeField] private float openHeight = 2f;
    [SerializeField] private float openingDuration = 1f;

    [Header("Interaction")] [SerializeField]
    private string playerTag;

    private float doorOpenness;
    private Vector3 doorStartPosition;

    private void Awake()
    {
        doorStartPosition = doorTransform.position;
    }

    private float DoorOpenness
    {
        get => doorOpenness;
        set
        {
            doorOpenness = Mathf.Clamp01(value);
            var doorPos = doorTransform.position;
            doorTransform.position = new Vector3
            (
                doorPos.x,
                doorStartPosition.y + (openHeight * doorOpenness),
                doorPos.z
            );
        }
    }

    private Coroutine currentAction;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag))
            OpenDoor();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(playerTag))
            CloseDoor();
    }

    private void OpenDoor()
    {
        if (currentAction != null)
            StopCoroutine(currentAction);
        currentAction = StartCoroutine(TweenOpenness(1f));
    }

    private void CloseDoor()
    {
        if (currentAction != null)
            StopCoroutine(currentAction);
        currentAction = StartCoroutine(TweenOpenness(0f));
    }

    private IEnumerator TweenOpenness(float finalValue)
    {
        var wait = new WaitForEndOfFrame();
        finalValue = Mathf.Clamp01(finalValue);
        float normalizedTime = DoorOpenness;
        bool up = finalValue > normalizedTime;
        while (normalizedTime <= 1f && normalizedTime >= 0f)
        {
            float delta = (Time.deltaTime / openingDuration);
            normalizedTime += up ? delta : -delta;
            DoorOpenness = normalizedTime;
            yield return wait;
        }
    }

    public void SwitchOn()
    {
        OpenDoor();
    }

    public void SwitchOff()
    {
        CloseDoor();
    }
}