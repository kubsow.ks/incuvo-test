﻿using System.Collections.Generic;
using UnityEngine;

/// Przycisk
public class Zadanie3_2 : MonoBehaviour
{
    [SerializeField] private List<GameObject> switchables = new List<GameObject>();

    private List<ISwitchable> switchableList = new List<ISwitchable>();

    private void Awake()
    {
        foreach (var gobject in switchables)
            switchableList.Add(gobject.GetComponent<ISwitchable>());
    }

    private void OnTriggerEnter(Collider other)
    {
        foreach (var switchable in switchableList)
            switchable.SwitchOn();
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (var switchable in switchableList)
            switchable.SwitchOff();
    }
}