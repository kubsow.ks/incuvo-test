﻿using UnityEngine;

/// Kontroler oświetlenia
public class Zadanie3_1 : MonoBehaviour, ISwitchable
{
    [Header("References")] [SerializeField]
    private Light light;

    [Header("Colors")] [SerializeField] private Color lightOn;
    [SerializeField] private Color lightOff;
    
    public void SwitchOn()
    {
        light.color = lightOn;
    }

    public void SwitchOff()
    {
        light.color = lightOff;
    }
}